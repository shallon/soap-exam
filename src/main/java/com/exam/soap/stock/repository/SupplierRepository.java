package com.exam.soap.stock.repository;

import com.exam.soap.stock.bean.Item;
import com.exam.soap.stock.bean.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SupplierRepository extends JpaRepository<Supplier,Integer> {
    Supplier findSupplierById(Integer id);
    List<Supplier> findSupplierByNamesOrEmailOrMobile(String name, String email, String mobile);
    Supplier findSupplierByNames(String names);
}
