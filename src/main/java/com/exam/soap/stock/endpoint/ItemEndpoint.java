package com.exam.soap.stock.endpoint;

import com.exam.soap.stock.bean.Item;
import com.exam.soap.stock.bean.Supplier;
import com.exam.soap.stock.repository.ItemRepository;
import com.exam.soap.stock.repository.SupplierRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import output.*;

import java.util.List;

@Endpoint
public class ItemEndpoint {

    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private SupplierRepository supplierRepository;

    @PayloadRoot(namespace = "http://soap/items",localPart = "GetItemDetailsRequest")
    @ResponsePayload
    public GetItemDetailsResponse findItem(@RequestPayload GetItemDetailsRequest request)
    {
        Item item= itemRepository.findById(request.getId()).get();
        GetItemDetailsResponse itemDetailsResponse = mapItemDetails(item);
        return itemDetailsResponse;
    }



    @PayloadRoot(namespace = "http://soap/items",localPart = "GetAllItemDetailsRequest")
    @ResponsePayload

    public GetAllItemDetailsResponse getAllItems(@RequestPayload GetAllItemDetailsRequest request)
    {
        GetAllItemDetailsResponse itemResp = new GetAllItemDetailsResponse();
        List<Item> items = itemRepository.findAll();
        for(Item item: items){
            GetItemDetailsResponse itemDetailsResponse = mapItemDetails(item);
            itemResp.getItemDetails().add(itemDetailsResponse.getItemDetails());
        }
        return  itemResp;
    }



    @PayloadRoot(namespace = "http://soap/items", localPart = "CreateItemDetailsRequest")
    @ResponsePayload
    public CreateItemDetailsResponse save(@RequestPayload CreateItemDetailsRequest request) {

        Supplier supplier = supplierRepository.findById(request.getItemDetails().getSupplierId()).get();

        Item testItem = itemRepository.save(new Item(
                request.getItemDetails().getId(),
                request.getItemDetails().getName(),
                request.getItemDetails().getItemCode(),
                request.getItemDetails().getPrice(),
                supplier
        ));
        CreateItemDetailsResponse itemDetailsResponse = new CreateItemDetailsResponse();
        itemDetailsResponse.setItemDetails(request.getItemDetails());
        itemDetailsResponse.setMessage("Created Successfully");
        return itemDetailsResponse;
    }

    @PayloadRoot(namespace = "http://soap/items", localPart = "UpdateItemDetailsRequest")
    @ResponsePayload
    public UpdateItemDetailsResponse updateItem(@RequestPayload UpdateItemDetailsRequest request){
        UpdateItemDetailsResponse response = new UpdateItemDetailsResponse();

        Item item = itemRepository.findById(request.getItemDetails().getId()).get();
        Supplier supplier = supplierRepository.findById(request.getItemDetails().getSupplierId()).get();

        item.setItemCode(request.getItemDetails().getItemCode());
        item.setName(request.getItemDetails().getName());
        item.setPrice(request.getItemDetails().getPrice());
        item.setSupplier(supplier);
        itemRepository.save(item);
        response.setMessage("Item updated Successfully");
        return response;

    }

    @PayloadRoot(namespace = "http://soap/items", localPart = "DeleteItem")
    @ResponsePayload
    public DeleteItemDetailsResponse delete(@RequestPayload DeleteItemDetailsRequest request) {

        itemRepository.deleteById(request.getId());
        DeleteItemDetailsResponse itemDetailsResponse = new DeleteItemDetailsResponse();
        itemDetailsResponse.setMessage("Deleted Successfully");
        return itemDetailsResponse;
    }



    private GetItemDetailsResponse mapItemDetails(Item item){
        ItemDetails details = mapItem(item);
        GetItemDetailsResponse itemDetailsResponse = new GetItemDetailsResponse();
        itemDetailsResponse.setItemDetails(details);
        return itemDetailsResponse;
    }

    private UpdateItemDetailsResponse mapUpdateItemDetails(Item item, String message) {
        ItemDetails details = mapItem(item);
        UpdateItemDetailsResponse resp = new UpdateItemDetailsResponse();
        resp.setItemDetails(details);
        resp.setMessage(message);
        return resp;
    }

    private ItemDetails mapItem(Item item){
        ItemDetails itemDetails = new ItemDetails();
        itemDetails.setItemCode(item.getItemCode());
        itemDetails.setName(item.getName());
        itemDetails.setPrice(item.getPrice());
        itemDetails.setSupplierId(item.getSupplier().getId());
        return itemDetails;
    }
}
