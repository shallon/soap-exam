package com.exam.soap.stock.serviceImp;

import com.exam.soap.stock.bean.Supplier;
import com.exam.soap.stock.repository.SupplierRepository;
import com.exam.soap.stock.services.ISupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SupplierServiceImplement implements ISupplierService {

    @Autowired
    private SupplierRepository supplierRepository;

    @Override
    public List<Supplier> getAllSuppliers() {
       List<Supplier> list = new ArrayList<>();
        supplierRepository.findAll().forEach(e->list.add(e));
        return list;
    }

    @Override
    public Supplier getSupplierById(Integer supplierId) {
        Supplier supplier = supplierRepository.findSupplierById(supplierId);
        return supplier;
    }

    @Override
    public boolean addSupplier(Supplier supplier) {
       List<Supplier> checkSupplierExists = supplierRepository.findSupplierByNamesOrEmailOrMobile(supplier.getNames(),supplier.getEmail(),supplier.getMobile());
        if(checkSupplierExists.size() > 0){
            return false;
        }else{
            supplierRepository.save(supplier);
            return true;
        }
    }

    @Override
    public void updateSupplier(Supplier supplier) {

        supplierRepository.save(supplier);
    }

    @Override
    public void deleteSupplier(Supplier supplier) {
        supplierRepository.delete(supplier);

    }

}
